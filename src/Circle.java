public class Circle {
    private Point center;
    private double radius;

    public Circle(Point center, double radius) {
        this.center = center;
        this.radius = radius;
    }

    boolean containsPoint(Point p) {
        return p.distanceTo(this.center) < this.radius;
    }
    
     @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if ((object == null) || (object.getClass() != this.getClass())) {
                return false;
            }
            Circle circle = (Circle) object;
            return radius == circle.radius
                    && (center == circle.center
                    || (center != null && center.equals(circle.getCenter())));
        }
    
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + (int)radius;
            result = prime * result + ((center == null) ? 0 : center.hashCode());
            return result;
        }
    
        @Override
        public String toString() {
            return "Circle with center at the point ("
                    + center.getX()
                    + ", "
                    + center.getY()
                    + ") and radius r = "
                    + radius + ".";
        }
}