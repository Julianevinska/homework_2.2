import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Point> points = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        double answer;

        do {
            System.out.print("Введите координаты точки -\nx: ");
            double pX = sc.nextDouble();

            System.out.print("y: ");
            double pY = sc.nextDouble();

            points.add(new Point(pX, pY));

            System.out.println("Желаете добавить еще? (1 - да, 2 - нет)");
            answer = sc.nextInt();
        } while (answer == 1);

        System.out.println("Ваш выбор: 1");
        System.out.print("Введите координаты центра окружности и радиус.\nКоординаты центра окружности -\nx: ");
        double circleCenterX = sc.nextDouble();

        System.out.print("y: ");
        double circleCenterY = sc.nextDouble();

        System.out.println("Введите радиус окружности: ");
        double circleRadius = sc.nextDouble();

        Circle circle = new Circle(new Point(circleCenterX, circleCenterY), circleRadius);

        for (Point p : points) {
            if (circle.containsPoint(p)) {
                System.out.println(p.toString());
            }
        }
    }
}